# jigsaw vagrant

## Installation

* Clone repository
* Modify IP, hostname and hardware limits to your needs
* Modiy hosted directories and hostnames to your needs
* Start vagrant `vagrant up || (vagrant provision && vagrant reload)`

## Stack

#### Based on homestead

* Ubuntu 18.04
* Git
* PHP 7.2
* PHP 7.1
* PHP 7.0
* PHP 5.6
* Nginx
* Apache (Optional)
* MySQL
* MariaDB (Optional)
* Sqlite3
* PostgreSQL
* Composer
* Node (With Yarn, Bower, Grunt, and Gulp)
* Redis
* Memcached
* Beanstalkd
* Mailhog
* Elasticsearch (Optional)
* ngrok
* wp-cli
* Zend Z-Ray
* Go
* Minio

## License

    The MIT License (MIT)

    Copyright (c) 2018 Joshua Behrens

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
